from setuptools import setup

setup(name="trello_to_protocol",
      version="0.1",
      description="Generates a protocol from trello to markdown",
      url="",
      author="Margarete Drexler",
      author_email="",
      license="MIT",
      packages=["trello_to_protocol"],
      install_requires=[
            "pyyaml",
            "py-trello",
            "appdirs",
      ],
      zip_safe=False)
