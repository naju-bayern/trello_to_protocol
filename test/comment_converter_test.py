import unittest
import trello_to_protocol.comment_converter as comment_converter


class CommentConverterTest(unittest.TestCase):
    def test_convert_text(self):
        comment = {"data": {"text": "# Test123"}}
        result = comment_converter.converte_text(comment)
        self.assertEqual(result, "## Test123\n")

    def test_convert_empty_text(self):
        comment = {"data": {"text": ""}}
        result = comment_converter.converte_text(comment)
        self.assertEqual(result, "\n")

    def test_convert_multiline_text(self):
        comment = {"data": {"text": "# Test123\n## Headline 2"}}
        result = comment_converter.converte_text(comment)
        self.assertEqual(result, "## Test123\n### Headline 2\n")


if __name__ == '__main__':
    unittest.main()
