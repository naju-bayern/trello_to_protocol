from trello import TrelloClient
import unittest
import trello_to_protocol.card_converter as card_converter
import trello_to_protocol.board_converter as board_converter
import os

client = TrelloClient(
    api_key=os.environ["TRELLO_API_KEY"],
    api_secret=os.environ["TRELLO_TOKEN"]
)
test_board = board_converter.get_board_for_name(os.environ["TRELLO_TEST_BOARD_NAME"], client)
test_cards=test_board.get_cards()


def get_card_for_name(search_name):
    return next(filter(lambda card: card.name == search_name, test_cards))

class CardConverterTest(unittest.TestCase):


    def test_basic(self):
        hundred_years = 60 * 60 * 24 * 365 * 100
        test_card =get_card_for_name("Test basic comment")
        result = card_converter.convert_card(test_card, hundred_years)
        self.assertEqual("""# Test basic comment
List:

* Option A
* Option B

**Bold**
## Hallo
### du

* Blabla
* **Lustig**

""", result)


    def test_checklist(self):
        hundred_years = 60 * 60 * 24 * 365 * 100
        test_card = get_card_for_name("Checklists")
        result = card_converter.convert_card(test_card, hundred_years)
        self.assertEqual("""# Checklists
Hello

## ToDo: 

* [x] Hallo
* [ ] Du

## visible: 

* [ ] oma
* [ ] opa

""", result)

if __name__ == '__main__':
    unittest.main()
