# "Trello to protocol"- converter

Python package that generates a markdown-protocol from the comments on your trello-cards.
Each card is converted to a topic in the protocol. Markdown formatting in comments is supported.
You can define a timeframe to filter the comments in order to generate the protocol
 only for your last meeting.
 
 ## Install
 
 ```
# Clone this repo
git clone 

cd trello_to_protocol

#install
pip install ./
``` 

If you are on linux you may want to use `pip3` instead of `pip`.
 
 ## Usage
 For usage you have to create a `trello_to_protocol_conf.yaml` file with following content:
 ```
trello_key: <YOUR TRELLO KEY>
trello_token: <YOUR TRELLO TOKEN>
```
You get the keys at: https://trello.com/app-key

```
python -m trello_to_protocol
```
 
## Tests
 The tests require the following environment variables:
 
 * `TRELLO_API_KEY`
 * `TRELLO_TOKEN`
 * `TRELLO_TEST_BOARD_NAME = trello_to_protocol Test Board`
 

Add [this](https://trello.com/b/R3J7juYQ/trellotoprotocol-test-board) board to your trello-account to make the test work.