def converte_text(comment):
    """
    extracts the text of a comment and formats the headlines right
    :param comment: trello-comment
    :return: text of the trello-comment
    """
    comment_text=comment["data"]["text"]
    lines=comment_text.split("\n")
    lines_converted=[]
    for line in lines:
        if len(line)!=0 and line[0] == "#":
            line="#"+line
        lines_converted.append(line)
    return "\n".join(lines_converted) + "\n"