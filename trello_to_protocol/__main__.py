import trello_to_protocol.board_converter as board_converter
import trello_to_protocol.conf as conf

TRELLO_KEY = "trello_key"
TRELLO_TOKEN = "trello_token"

conf = conf.get()
board_converter.convert_board("Lajulei", 60 * 60 * 27*3, conf.get(TRELLO_KEY), conf.get(TRELLO_TOKEN))
