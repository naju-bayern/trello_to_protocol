import yaml
from pathlib import PurePath
from appdirs import AppDirs

TRELLO_KEY = "trello_key"
TRELLO_TOKEN = "trello_token"


def get():
    """
    Reads and verifies the configuration file.
    Exits in case of a nonexistent or invalid configuration.
    :return: A dictionary of configuration options.
    """
    conf = __load_conf()
    __verify_conf(conf)

    return conf

def __load_conf():
    dirs = AppDirs("trello_to_protocol", "naju-bayern")

    config_location = PurePath(dirs.user_config_dir) / "trello_to_protocol_conf.yaml"

    try:
        with open(str(config_location), "r") as conf_file:
            return yaml.full_load(conf_file)
    except FileNotFoundError:
        print(f"Config file not found. Please put it at {config_location}.")
        exit(2)


def __verify_conf(conf):
    if not TRELLO_KEY in conf or conf.get(TRELLO_KEY) == None:
        print("Please set your trello_key in your trello_to_protocol_conf.yaml file")
        exit(2)
    if not TRELLO_TOKEN in conf or conf.get(TRELLO_TOKEN) == None:
        print("Please set your trello_token in your trello_to_protocol_conf.yaml file")
        exit(2)
