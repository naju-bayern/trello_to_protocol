from trello import TrelloClient
import trello_to_protocol.card_converter as card_converter
from pathlib import Path


def get_board_for_name(name, trello_client):
    """"
    Searches fitting trello-board for name
    :param name: is name of the trello-board
    :param trello_client: trello-client
    :return trello-board
    :rtype: trello.Board
    """
    boards = trello_client.list_boards()
    for board in boards:
        if board.name == name:
            return board


def convert_board(board_name, max_age, key, token):
    client = TrelloClient(api_key=key, api_secret=token)
    board = get_board_for_name(board_name, client)
    cards = board.all_cards()
    protocol_path = str(Path.cwd().joinpath("protocol.md"))
    protocol=open(protocol_path, "x", encoding='utf8')
    for card in cards:
        card_text=card_converter.convert_card(card, max_age)
        protocol.write(card_text)
    protocol.close()