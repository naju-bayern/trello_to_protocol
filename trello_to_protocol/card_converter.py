import datetime
import trello_to_protocol.comment_converter as comment_converter
import trello as trello
import pytz


def __get_date_of_comment(comment):
    """
    Gives last edited date, if not possible comment date.
    :param comment:
    :rtype: datetime
    """
    if "dateLastEdited" in comment["data"]:
        date_string = comment["data"]["dateLastEdited"]
    else:
        date_string = comment["date"]
    return pytz.utc.localize(datetime.datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S.%fZ'))


def __select_recent_comments(comments, max_age):
    """
    Checks if comment dates are older then max_age and only returns those.
    :param comments: list of trello comments
    :return: list of fitting comments
    """
    date_now = datetime.datetime.now(pytz.utc)

    def is_comment_new(comment):
        comment_date = __get_date_of_comment(comment)
        age_in_seconds = (date_now - comment_date).total_seconds()
        return age_in_seconds < max_age

    recent_comments = list(filter(is_comment_new, comments))
    return recent_comments


def __convert_checklist(checklists):
    """
    Converts trello card checklists into markdown string.
    :param checklists: trello card checklist
    :return: markdown string
    :rtype str
    """
    relevant_checklists = list(
        filter(lambda checklist: checklist.name.lower().replace(" ", "") == "todo" or "(p)" in checklist.name.lower(),
               checklists))
    checklist_texts = ""
    for checklist in relevant_checklists:
        checklist_name = checklist.name.replace("(P)", "").replace("(p)", "").strip()
        checklist_texts = checklist_texts + "\n## " + checklist_name + ": \n\n"
        for item in checklist.items:
            checklist_texts += ("* [x] " if item["checked"] else "* [ ] ") + item["name"] + "\n"
    return checklist_texts


def __is_card_new(card, max_age):
    """
    Checks if card dates are older then max_age, used for quickly sort out old cards, as loading comments takes very long.
    :param card: trello-card
    :param max_age: Specifies, how new comments must be for being included in protocol.
    :return: True, if card is new enough
    :rtype: bool
    """
    date_now = datetime.datetime.now(pytz.utc)
    age_in_seconds = (date_now - card.date_last_activity).total_seconds()
    return age_in_seconds < max_age


def convert_card(card, max_age):
    """
    Converts card name, comment text and checklists into a markdown string.
    :param card: trello-card
    :type card: trello.Card
    :param max_age: Specifies, how new comments must be for being included in protocol.
    :return: markdown string
    """
    if not __is_card_new(card, max_age):
        return ""
    comments = card.comments
    recent_comments = __select_recent_comments(comments, max_age)
    if len(recent_comments) == 0:
        return ""
    headline = "# " + card.name + "\n"
    comment_texts = ""
    for recent_comment in recent_comments:
        comment_texts = comment_texts + comment_converter.converte_text(recent_comment)
    checklist_texts = __convert_checklist(card.checklists)

    return headline + comment_texts + checklist_texts + "\n"
